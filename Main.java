import java.util.*;


/*
 * Maxim Milovanov
 * */
public class Main { 
    public static void main(String[] args) { 

    	List<String> res = findPalindromesSimple("awdwag");
    	
    	for (String s : res) {
    		System.out.println(s);
    	}
    } 
    
    
    /**
     * ����� ���������� �� O(n^2)
     * */
    public static List<String> findPalindromesSimple(String st) {
    	List<String> res = new ArrayList<>();
    	
    	int n = st.length();
    	for (int i=0; i<n; i++) {
    		
    		int len = 1;	//����� ����� ����� ��������� ���������� (���������� = 1, �.�. ��� ������)
    		//��������� � ����� ������� - �������� �����
    		while ( (i-len >= 0) && (i+len < n) &&
    				(st.charAt(i-len) == st.charAt(i+len)) ) {	//��������� �� ����� �� �� ������� ������, ���������� ������� �������
    			res.add( st.substring(i-len, i+len+1) );
    			len++;
    		}
    		
    		len = 0; //����� ������� ����������
    		//��������� � ����� ������� - �������� �����
    		while ( (i-len >= 0) && (i+len+1 < n) &&
    				(st.charAt(i-len) == st.charAt(i+len+1)) ) {	//��������� �� ����� �� �� ������� ������, ���������� ������� �������
    			res.add( st.substring(i-len, i+len+2) );
    			len++;
    		}
    	}
    	
    	
    	return res;
    }




    
} 
